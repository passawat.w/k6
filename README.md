# K6 Load Test

How to run:
```
    docker compose up -d
```

http://localhost:3000/

import dashboard(2587) if not exist
make sure to add the influxdb datasource

Run service:
```
    go run .
```

Run load test:
```
    docker compose run k6 run /scripts/test.js    
```

Check useCache:
```
    curl localhost:8000/products
```